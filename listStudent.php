<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="./studentStyle.css">
    <title>Document</title>
</head>

<body>
    <div class="listStudent">
        <div class="formSearch">
            <form>
                <div class="formSearch__data d-flex">
                    <div class="searchLabel">
                        <label for="">Khoa</label>
                    </div>
                    <div class="searchInput">
                        <input id="department" type="text" value="">
                    </div>
                </div>
                <div class="formSearch__data d-flex">
                    <div class="searchLabel">
                        <label for="">Từ khoá</label>
                    </div>
                    <div class="searchInput">
                        <input id="keySearch" type="text">
                    </div>
                </div>
                <div class="submitData">
                    <button id="search">Tìm kiếm</button>
                </div>
            </form>
        </div>
        <div class="showList">
            <div class="showListWrap d-flex">
                <div class="listValue " style="width:30%; margin-left:20px">
                    <p>Số sinh viên tìm thấy</p>
                </div>
                <div class="addValue">
                    <button id="redirectStd">Thêm</button>
                </div>
            </div>
            <div class="listData">
                <div class="dataTable">
                    <table>
                        <tr>
                            <th>No</th>
                            <th>Tên sinh viên</th>
                            <th>Khoa</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Nguyễn Văn A</td>
                            <td>Khoa học máy tính</td>
                            <td class="dataTable__Manage d-flex">
                                <div class="dataTable__Manage--Delete"><button onclick="deleteValue()">Xoá</button></div>
                                <div class="dataTable__Manage--Edit"><button>Sửa</button></div>
                            </td>

                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Trần Thị B</td>
                            <td>Khoa học máy tính</td>
                            <td class="dataTable__Manage d-flex">
                                <div class="dataTable__Manage--Delete"><button onclick="deleteValue()">Xoá</button></div>
                                <div class="dataTable__Manage--Edit"><button>Sửa</button></div>
                            </td>

                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Nguyễn Hoàng C</td>
                            <td>Khoa học vật liệu</td>
                            <td class="dataTable__Manage d-flex">
                                <div class="dataTable__Manage--Delete"><button onclick="deleteValue()">Xoá</button></div>
                                <div class="dataTable__Manage--Edit"><button>Sửa</button></div>
                            </td>

                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Nguyễn Duy Hoàng</td>
                            <td>Master Node Js</td>
                            <td class="dataTable__Manage d-flex">
                                <div class="dataTable__Manage--Delete"><button onclick="deleteValue()">Xoá</button></div>
                                <div class="dataTable__Manage--Edit"><button>Sửa</button></div>
                            </td>

                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<script>
    document.getElementById("redirectStd").onclick = function() {
        location.href = "signUp.php";
    };
    const depart = document.getElementById("department");
    const key = document.getElementById("keySearch");
    depart.addEventListener("change", updateValue);
    key.addEventListener("change", updateKeySearch);

    function updateValue(e) {
        localStorage.setItem("value1", e.target.value);
    }

    function updateKeySearch(e) {
        localStorage.setItem("value2", e.target.value)

    }
    document.body.onload = () => {
        document.getElementById("department").value = localStorage.getItem("value1");
        document.getElementById("keySearch").value = localStorage.getItem("value2");
    }


    deleteValue = () => {
        var depart = (document.getElementById("department").value = "");
        var key = (document.getElementById("keySearch").value = "");
        localStorage.removeItem("value1");
        localStorage.removeItem("value2");
    };
</script>